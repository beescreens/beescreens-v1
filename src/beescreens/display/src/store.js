/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import { socket, peer } from './server';

Vue.use(Vuex);

const vuexLocal = new VuexPersist({
    storage: window.localStorage,
});

export default new Vuex.Store({
    state: {
        name: null,
        url: null,
        location: null,
        password: null,
        port: null,
        protocol: null,
        jwt: null,
        displayerId: null,
        connected: false,
    },
    mutations: {
        configure(state, config) {
            const {
                name, url, port, password, location, protocol,
            } = config;
            state.name = name;
            state.url = url;
            state.port = port;
            state.protocol = protocol;
            state.password = password;
            state.location = location;
        },
        connected(state, value) {
            state.connected = value;
        },
        serverConnected(state, data) {
            state.jwt = data.jwt;
            state.displayerId = data.id;
        },
        clear(state) {
            state.name = null;
            state.url = null;
            state.jwt = null;
            state.connected = false;
        },
    },
    getters: {
        ready(state) {
            return state.connected;
        },
        endpoint(state) {
            return `${state.protocol}//${state.url}:${state.port}/api/displayers`;
        },
    },
    actions: {
        connect(context) {
            return new Promise((resolve, reject) => {
                context.dispatch('getJwt').then(() => {
                    context.dispatch('connectSocket').then(() => {
                        context.dispatch('connectPeer').then(() => {
                            console.log('connected to server and ready to roll!');
                            context.commit('connected', true);
                            resolve();
                        }).catch(() => {
                            reject('Failed connecting to peer');
                        });
                    }).catch(() => {
                        reject('Failed connecting to socket');
                    });
                }).catch(() => {
                    reject('Failed getting JWT');
                });
            });
        },
        getJwt(context) {
            return new Promise((resolve, reject) => {
                fetch(context.getters.endpoint, {
                    method: 'POST',
                    headers: { 'content-type': 'application/json' },
                    body: JSON.stringify({
                        name: context.state.name,
                        location: context.state.location,
                        password: context.state.password,
                        width: window.innerWidth,
                        height: window.innerHeight,
                    }),
                }).then((res) => {
                    res.json().then((data) => {
                        if (data.jwt && data.id) {
                            context.commit('serverConnected', data);
                            resolve();
                        } else reject();
                    });
                }).catch((error) => {
                    console.log(error);
                    reject();
                });
            });
        },
        connectSocket({ state, getters }) {
            return socket.connect(getters.endpoint, state.jwt);
        },
        connectPeer({ state }) {
            return peer.connect(state.displayerId, {
                port: state.port,
                host: state.url,
                path: '/api/peers',
            });
        },
    },
    plugins: [vuexLocal.plugin],
});
