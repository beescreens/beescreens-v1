const path = require('path');

module.exports = {
    baseUrl: './',
    outputDir: path.resolve(__dirname, '../dist/public/admin'),

    lintOnSave: false,

    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false,
        },
    },
};
