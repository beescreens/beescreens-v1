import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

const vuexLocal = new VuexPersist({
    storage: window.localStorage
})

export const store = new Vuex.Store({

    state : {
        user : null,
    },

    getters: {
        getUser(state){
            return state.user;
        }
    },

    mutations : {
        setUser(state, newUser){
            state.user = newUser;
        }
    },

    plugins: [vuexLocal.plugin]

});