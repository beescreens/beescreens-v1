import { authHeader } from './auth-header';

function getBaseUrl() {
    const baseDebug = 'localhost:8080/api';

    // console.log(`base url : ${process.env.VUE_APP_API_HOSTNAME}`);

    if (process.env.VUE_APP_API_HOSTNAME === undefined) {
        return baseDebug;
    }
    return process.env.VUE_APP_API_HOSTNAME;
}

function logout() {
    // remove user from local storage to log user out
    // eslint-disable-next-line
    localStorage.removeItem('user');
    localStorage.removeItem('vuex');
}

function getRequestOptions() {
    // Requests GET options
    const requestOptions = {
        method: 'GET',
        headers: { Authorization: authHeader() },
    };
    return requestOptions;
}

function postRequestOption(body) {
    // Header
    // eslint-disable-next-line
    const header = new Headers({
        'Content-Type': 'application/json',
        Authorization: authHeader(),
    });

    // Request options
    const requestOptions = {
        method: 'POST',
        headers: header,
        body,
    };

    return requestOptions;
}

function patchRequestOption(body) {
    // Header
    // eslint-disable-next-line
    const header = new Headers({
        'Content-Type': 'application/json',
        Authorization: authHeader(),
    });

    // Request options
    const requestOptions = {
        method: 'PATCH',
        headers: header,
        body,
    };

    return requestOptions;
}

function deleteRequestOptions() {
    // Requests GET options
    const requestOptions = {
        method: 'DELETE',
        headers: { Authorization: authHeader() },
    };
    return requestOptions;
}


// Used to parse data for the login
function handleResponseJSON(response) {
    return response.text().then((text) => {
        console.log(response);
        console.log(text);

        const data = text && JSON.parse(text);

        // Remove the token if error status
        if (!response.ok) {
            if (response.status === 403) {
                // auto logout if 403 response returned from api
                logout();
            }
            // const error = (data && data.message) || response.statusText;
            return Promise.reject(response);
        }
        return data;
    });
}

// Login
function login(username, password) {
    // eslint-disable-next-line
    const url = `http://${getBaseUrl()}/login`;
    console.log(url);

    const mybody = JSON.stringify({ username, password });
    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody))
        .then(handleResponseJSON)
        .then((user) => {
            // Login successful if there's a jwt token in the response
            if (user.jwt) {
                // store user details and jwt token in
                // local storage to keep user logged in between page refreshes
                // eslint-disable-next-line
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
        });
}

// Get the displayers
function getDisplayers() {
    const url = `http://${getBaseUrl()}/displayers`;

    return fetch(url, getRequestOptions());
}

// Get users
function getUsers() {
    const url = `http://${getBaseUrl()}/users`;

    // eslint-disable-next-line
    return fetch(url, getRequestOptions());
}

// Get displayer
function getDisplayer(name) {
    const url = `http://${getBaseUrl()}/displayer/${name}`;

    // eslint-disable-next-line
    return fetch(url, getRequestOptions());
}

// Get user
function getUser(username) {
    const url = `http://${getBaseUrl()}/users/${username}`;
    // eslint-disable-next-line
    return fetch(url, getRequestOptions());
}

// Create a user
function postUser(username, password, role) {
    const url = `http://${getBaseUrl()}/users`;

    const mybody = JSON.stringify({ username, password, role });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

// Create a user
function postDisplayer(name, location, width, height, password) {
    const url = `http://${getBaseUrl()}/displayers`;

    const mybody = JSON.stringify({
        name, location, width, height, password,
    });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

// Delete user
function deleteUser(username) {
    const url = `http://${getBaseUrl()}/users/${username}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Delete displayer
function deleteDisplayer(name) {
    const url = `http://${getBaseUrl()}/displayers/${name}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Patch a user
function patchUser(username, password, role) {
    const url = `http://${getBaseUrl()}/users/${username}`;

    const mybody = JSON.stringify({ credentials: { username, password }, role });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

// Patch a displayer (don't work)
function patchDisplayer(name, location, width, height, status) {
    const url = `http://${getBaseUrl()}/displayers/${name}`;

    const mybody = JSON.stringify({
        name, location, width, height, status,
    });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

function postMessage(title, content, level) {
    const url = `http://${getBaseUrl()}/messages`;

    const mybody = JSON.stringify({ title, content, level });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

function postMessageApplication(title, content, level, applicationName) {
    const url = `http://${getBaseUrl()}/messages/applications/${applicationName}`;

    const mybody = JSON.stringify({ title, content, level });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

function postMessageDisplayer(title, content, level, displayerName) {
    const url = `http://${getBaseUrl()}/messages/displayers/${displayerName}`;

    const mybody = JSON.stringify({ title, content, level });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

function postMessagePlayer(title, content, level, username) {
    const url = `http://${getBaseUrl()}/messages/players/${username}`;

    const mybody = JSON.stringify({ title, content, level });

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}

// Get Players
function getPlayers() {
    const url = `http://${getBaseUrl()}/players`;

    // eslint-disable-next-line
    return fetch(url, getRequestOptions());
}

// Get Player
function getPlayer(username) {
    const url = `http://${getBaseUrl()}/players/${username}`;

    // eslint-disable-next-line
    return fetch(url, getRequestOptions());
}

// Delete player
function deletePlayer(username) {
    const url = `http://${getBaseUrl()}/players/${username}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Patch player
function patchPlayer(username, status) {
    const url = `http://${getBaseUrl()}/players/${username}`;

    const mybody = JSON.stringify({ username, status });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

// Get Applications
function getApplications() {
    const url = `http://${getBaseUrl()}/apps`;

    return fetch(url, getRequestOptions());
}

// Get Application
function getApplication(name) {
    const url = `http://${getBaseUrl()}/apps/${name}`;

    return fetch(url, getRequestOptions());
}

// Delete Application
function deleteApplication(name) {
    const url = `http://${getBaseUrl()}/apps/${name}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Patch Application
function patchApplication(name, status) {
    const url = `http://${getBaseUrl()}/apps/${name}`;

    const mybody = JSON.stringify({ status });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

// Get Sessions
function getSessions() {
    const url = `http://${getBaseUrl()}/sessions`;

    return fetch(url, getRequestOptions());
}

// Get Session
function getSession(id) {
    const url = `http://${getBaseUrl()}/sessions/${id}`;

    return fetch(url, getRequestOptions());
}

// Delete Session
function deleteSession(id) {
    const url = `http://${getBaseUrl()}/sessions/${id}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Patch Session (le body est faux)
function patchSession(namedisplayer, id) {
    const url = `http://${getBaseUrl()}/sessions/${id}`;

    const mybody = JSON.stringify({ displayer: namedisplayer });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

// Get Queue
function getQueue() {
    const url = `http://${getBaseUrl()}/queue`;

    return fetch(url, getRequestOptions());
}

// Get element from position
function getQueuePosition(position) {
    const url = `http://${getBaseUrl()}/queue/${position}`;

    return fetch(url, getRequestOptions());
}

// Delete position element in a queue
function deleteQueuePosition(position) {
    const url = `http://${getBaseUrl()}/queue/${position}`;

    // eslint-disable-next-line
    return fetch(url, deleteRequestOptions());
}

// Patch queue element
function patchQueuePosition(position, newPosition) {
    const url = `http://${getBaseUrl()}/queue/${position}`;

    const mybody = JSON.stringify({ position: newPosition });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

// Get Options
function getOptions() {
    const url = `http://${getBaseUrl()}/options`;

    return fetch(url, getRequestOptions());
}

// Patch Options
function patchOption(id, value) {
    const url = `http://${getBaseUrl()}/options/${id}`;

    const mybody = JSON.stringify({ value });

    // eslint-disable-next-line
    return fetch(url, patchRequestOption(mybody));
}

function postPlay(name) {
    const url = `http://${getBaseUrl()}/play/${name}`;

    const mybody = JSON.stringify({ }); // Nothing ?

    // eslint-disable-next-line
    return fetch(url, postRequestOption(mybody));
}


export const userService = {
    login,
    logout,

    getOptions,
    patchOption,

    postPlay,

    getUsers,
    postUser,
    getUser,
    deleteUser,
    patchUser,

    getDisplayers,
    postDisplayer,
    getDisplayer,
    deleteDisplayer,
    patchDisplayer,

    postMessage,
    postMessageApplication,
    postMessageDisplayer,
    postMessagePlayer,

    getPlayers,
    getPlayer,
    deletePlayer,
    patchPlayer,

    getApplications,
    getApplication,
    deleteApplication,
    patchApplication,

    getSessions,
    getSession,
    deleteSession,
    patchSession,

    getQueue,
    getQueuePosition,
    deleteQueuePosition,
    patchQueuePosition,

};
