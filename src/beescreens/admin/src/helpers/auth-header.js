export function authHeader() {
    // return authorization header with jwt token
    const token = JSON.parse(localStorage.getItem('user'));

    if (token && token.jwt) {
        return `Bearer ${token.jwt}`;
    }
    return '';
}
