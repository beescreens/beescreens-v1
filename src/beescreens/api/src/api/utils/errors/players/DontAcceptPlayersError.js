module.exports = class extends Error {
    constructor() {
        super('The server doesn\'t accept new players.');

        Error.captureStackTrace(this, this.constructor);
    }
};
