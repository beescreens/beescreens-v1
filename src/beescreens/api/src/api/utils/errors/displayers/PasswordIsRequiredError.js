module.exports = class extends Error {
    constructor() {
        super('The password is required.');

        Error.captureStackTrace(this, this.constructor);
    }
};
