module.exports = class extends Error {
    constructor() {
        super('The server only accept new displayers.');

        Error.captureStackTrace(this, this.constructor);
    }
};
