module.exports = class extends Error {
    constructor() {
        super('The provided password is incorrect.');

        Error.captureStackTrace(this, this.constructor);
    }
};
