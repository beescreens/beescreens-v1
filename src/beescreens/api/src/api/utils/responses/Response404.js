module.exports = class {
    constructor() {
        this.code = 404;
    }

    send(res) {
        const { code } = this;

        res.status(code);

        res.end();
    }
};
