module.exports = class {
    constructor() {
        this.code = 401;
    }

    send(body, res) {
        const { code } = this;

        res.status(code);
        res.json(body);

        res.end();
    }
};
