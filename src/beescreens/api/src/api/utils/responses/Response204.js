module.exports = class {
    constructor() {
        this.code = 204;
    }

    send(res) {
        const { code } = this;

        res.status(code);

        res.end();
    }
};
