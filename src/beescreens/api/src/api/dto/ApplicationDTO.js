class ApplicationDTO {
    constructor(application) {
        this.name = application.name;
        this.logo = application.logo;
        this.description = application.description;
        this.version = application.version;
        this.homepage = application.homepage;
        this.authors = application.authors;
        this.status = application.status;
    }
}

module.exports = () => ApplicationDTO;
