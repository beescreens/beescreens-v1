class DisplayerDTO {
    constructor(displayer) {
        this.name = displayer.name;
        this.location = displayer.location;
        this.width = displayer.width;
        this.height = displayer.height;
        this.status = displayer.status;
    }
}

module.exports = () => DisplayerDTO;
