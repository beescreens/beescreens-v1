class OptionDTO {
    constructor(option) {
        this.id = option.id;
        this.name = option.name;
        this.description = option.description;
        this.value = option.value;
    }
}

module.exports = () => OptionDTO;
