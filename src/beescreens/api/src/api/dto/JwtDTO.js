class JwtDTO {
    constructor(ip) {
        this.ip = ip.ip;
        this.timestamp = ip.timestamp;
    }
}

module.exports = () => JwtDTO;
