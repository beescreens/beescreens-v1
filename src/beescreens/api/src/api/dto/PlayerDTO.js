class PlayerDTO {
    constructor(player) {
        this.username = player.username;
        this.status = player.status;
    }
}

module.exports = () => PlayerDTO;
