const { ExpressPeerServer } = require('peer');
const EventEmitter = require('events');

module.exports = class extends EventEmitter {
    /**
     * The constructor.
     */
    constructor(debug, restServer) {
        super();

        this.debug = debug('beescreens:peer-server');
        this.restServer = restServer;
    }

    /**
     * Start the server.
     */
    start() {
        const { debug, restServer } = this;

        const { express } = restServer;

        const instance = ExpressPeerServer(restServer.instance, {
            debug: true,
        });

        express.use('/api/peers', instance);

        instance.on('connection', (id) => {
            debug(`Connection from '${id}'`);
        });

        instance.on('disconnect', (id) => {
            debug(`Disconnect from '${id}'`);
        });

        this.instance = instance;

        debug('Ready for connections');
        this.emit('started');
    }

    /**
     * Stop the server.
     */
    stop() {
        const { debug } = this;
        // Intervals are not correctly cleaned in this object and
        // and prevents from closing the programm gracefully.
        //
        // instance._setCleanupIntervals);

        debug('Server gracefully stopped.');
        this.emit('stopped');
    }
};
