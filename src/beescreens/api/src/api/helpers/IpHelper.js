module.exports = ({ IpService, Response403 }) => {
    function verify(req, res, next) {
        const { ip } = req;

        IpService.isIpBlacklisted(ip)
            .then(() => Response403.send(res))
            .catch(() => IpService.isIpKnown(ip))
            .then(() => {
                req.ipWasKnown = true;
            })
            .catch(() => {
                req.ipWasKnown = false;
                IpService.addKnownIp(ip);
            })
            .finally(() => {
                if (req.ipWasKnown != null) {
                    next();
                }
            });
    }

    return {
        verify,
    };
};
