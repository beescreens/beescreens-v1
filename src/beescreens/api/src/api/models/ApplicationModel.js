class ApplicationModel {
    constructor(
        name,
        logo,
        description,
        version,
        homepage,
        authors,
        instance,
        status,
    ) {
        this.name = name;
        this.logo = logo;
        this.description = description;
        this.version = version;
        this.homepage = homepage;
        this.authors = authors;
        this.instance = instance;
        this.status = status;
    }

    send(messageType, payload, expectAnswer) {
        const { instance } = this;

        return instance.send(messageType, payload, expectAnswer);
    }

    sessionReady(userId, displayerId, params, callback) {
        const { instance } = this;

        return instance.sessionReady(userId, displayerId, params, callback);
    }
}

module.exports = () => ApplicationModel;
