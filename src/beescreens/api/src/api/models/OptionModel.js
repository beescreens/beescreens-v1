class OptionModel {
    constructor(
        id,
        name,
        description,
        value,
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.value = value;
    }
}

module.exports = () => OptionModel;
