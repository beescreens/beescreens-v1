class PlayerModel {
    constructor(socket, username, status) {
        this.socket = socket;
        this.username = username;
        this.status = status;
    }

    send(messageType, data) {
        const { socket } = this;

        return new Promise((resolve, reject) => {
            if (socket != null) {
                socket.emit(messageType, data);
                resolve();
            } else {
                reject();
            }
        });
    }
}

module.exports = () => PlayerModel;
