class UserModel {
    constructor(
        username,
        password,
        role,
        status,
    ) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
    }
}

module.exports = () => UserModel;
