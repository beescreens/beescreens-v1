const EventEmitter = require('events');
const mongoose = require('mongoose');

const { connection } = mongoose;

class Database extends EventEmitter {
    constructor(
        dbUsername,
        dbPassword,
        dbHostname,
        dbName,
        debug,
    ) {
        super();

        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
        this.dbHostname = dbHostname;
        this.dbName = dbName;

        this.dbConfig = {
            useNewUrlParser: true,
            useCreateIndex: true,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 500,
            auth: {
                authSource: 'admin',
            },
        };

        this.debug = debug('beescreens:database');
    }

    connect() {
        const {
            dbUsername,
            dbPassword,
            dbHostname,
            dbName,
            dbConfig,
            debug,
        } = this;

        connection.once('open', () => {
            this.emit('connected');
        });

        const tryToConnect = () => {
            mongoose.connect(`mongodb://${dbUsername}:${dbPassword}@${dbHostname}/${dbName}`, dbConfig)
                .then(
                    () => {
                        debug('Connection to database established.');
                    },
                    () => {
                        debug('Connection to database failed, retrying...');
                        setTimeout(tryToConnect, 500);
                    },
                );
        };

        tryToConnect();
    }

    disconnect() {
        const { debug } = this;

        mongoose.disconnect(() => {
            debug('Grafully disconnected from the database.');
            this.emit('disconnected');
        });
    }
}

module.exports = {
    Database,
    Mongoose: mongoose,
    MongooseConnection: connection,
};
