// TODO - is this good practice in DI
const uniqid = require('uniqid');

module.exports = class {
    constructor(debug, DisplayerService, PlayerService, QueueService, SessionModel, Status) {
        const sessions = new Map();

        this.debug = debug('beescreens:session-service');
        this.DisplayerService = DisplayerService;
        this.PlayerService = PlayerService;
        this.QueueService = QueueService;
        this.SessionModel = SessionModel;
        this.sessions = sessions;
        this.Status = Status;
    }

    canStartSession() {
        const { debug, DisplayerService, QueueService } = this;

        return new Promise((resolve, reject) => {
            let availableNumberOfDisplayers;
            let numberOfWaitingPlayers;

            let chosenPlayer;
            let chosenApplication;
            let chosenDisplayer;

            Promise.all([
                DisplayerService.getAvailableDisplayers()
                    .then((displayers) => {
                        availableNumberOfDisplayers = displayers.size;
                    }),
                QueueService.getQueueElements()
                    .then((queueElements) => {
                        numberOfWaitingPlayers = queueElements.length;
                    }),
            ])
                .then(() => {
                    debug(`Number of waiting players: ${numberOfWaitingPlayers}`);
                    debug(`Number of available displayers: ${availableNumberOfDisplayers}`);

                    if (availableNumberOfDisplayers > 0 && numberOfWaitingPlayers > 0) {
                        return Promise.all([
                            DisplayerService.getAvailableDisplayer()
                                .then((displayer) => {
                                    chosenDisplayer = displayer;
                                }),
                            QueueService.getNext()
                                .then((queueElement) => {
                                    const { player, application } = queueElement;

                                    chosenPlayer = player;
                                    chosenApplication = application;
                                }),
                        ]);
                    }

                    // Does that work..?
                    throw new Error();
                })
                .then(() => {
                    this.createSession(chosenPlayer, chosenApplication, chosenDisplayer)
                        .then(() => resolve());
                })
                .catch(() => {
                    debug('Cannot create session now');
                    reject();
                });
        });
    }

    deleteSessionForDisplayer(sessionId) {
        const session = this.getSession(sessionId);

        this.deleteSession(sessionId);

        this.DisplayerService.deleteDisplayer(session.displayer.name);
    }

    createSession(player, application, displayer) {
        const {
            ApplicationService,
            debug,
            SessionModel,
            sessions,
            Status,
        } = this;

        const id = uniqid();

        debug(`Session #${id} will link application '${application.name}', displayer '${displayer.name}' and player '${player.username}'`);

        return new Promise((resolve) => {
            const session = new SessionModel(id, player, application, displayer);

            sessions.set(id, session);

            let displayerReady = false;
            let playerReady = false;

            // eslint-disable-next-line no-param-reassign
            player.position = null;

            // eslint-disable-next-line no-param-reassign
            player.sessionId = id;

            // eslint-disable-next-line no-param-reassign
            displayer.sessionId = id;

            displayer.socket.on('session-end', () => {
                this.deleteSession(id);
            });

            player.socket.on('session-end', () => {
                this.deleteSession(id);
            });

            player.socket.on('player-ready', () => {
                debug(`Session #${id}: Player ${player.username} ready.`);
                playerReady = true;
            });

            displayer.socket.on('displayer-ready', () => {
                debug(`Session #${id}: Displayer ${displayer.name} ready.`);
                displayerReady = true;
            });

            player.socket.on('player-data', (payload) => {
                debug(`Session #${id}: Player is sending data to ${application.name}.`);
                application.instance.data(payload, player.username);
            });

            Promise.all([
                player.send('session-ready', {
                    application: application.name,
                    displayer: displayer.name,
                }),
                application.sessionReady(
                    player.username,
                    // Peer only accepts Alpha-numeric ID
                    displayer.name.replace(/[^A-Za-z0-9]/g, ''),
                    player.params,
                    (messageType, payload) => {
                        debug(`Session #${id}: Application is sending message '${messageType}'.`);
                        if (messageType === 'application-data') {
                            player.send('application-data', payload);
                        } else if (messageType === 'end-session') {
                            this.deleteSession();
                        } else if (messageType === 'disconnect') {
                            Promise.all([
                                this.askDisplayerToEndSession(id),
                                this.askPlayerToEndSession(id),
                                ApplicationService
                                    .updateApp(application.name, Status.INACTIVE.toString()),
                            ])
                                .then(() => {
                                    sessions.delete(id);

                                    this.canStartSession().catch(() => {});
                                });
                        }
                    },
                ),
                displayer.send('session-ready', {
                    player: player.username,
                    application: application.name,
                }),
            ])
                .then(() => {
                    // eslint-disable-next-line consistent-return
                    const interval = setInterval(() => {
                        if (playerReady && displayerReady) {
                            clearInterval(interval);
                            return Promise.all([
                                player.socket.emit('session-go'),
                                application.instance.sessionGo(),
                                displayer.socket.emit('session-go'),
                            ]);
                        }
                    }, 200);
                })
                .then(() => {
                    debug(`Player '${player.username}' will play '${application.name}' on displayer '${displayer.name}`);
                    resolve();
                });
        });
    }

    deleteSession(sessionId) {
        const { debug, sessions } = this;

        return new Promise((resolve) => {
            this.getSession(sessionId)
                .then((session) => {
                    const { player, application, displayer } = session;

                    debug(`Session for player '${player.username}', application '${application.name}' and displayer '${displayer.name} will end.`);

                    return Promise.all([
                        this.askPlayerToEndSession(player),
                        this.askDisplayerToEndSession(displayer),
                        this.askApplicationToEndSession(application),
                    ]);
                })
                .then(() => {
                    sessions.delete(sessionId);


                    this.canStartSession().catch(() => {});

                    resolve();
                });
        });
    }

    askApplicationToEndSession(sessionId) {
        const { sessions } = this;

        return new Promise((resolve) => {
            const session = sessions.get(sessionId);

            if (session != null) {
                const { application, displayer, player } = session;

                application.send('session-end', {
                    displayer: displayer.name,
                    player: player.username,
                })
                    .then(() => this.canStartSession())
                    .catch(() => {});
            }

            resolve();
        });
    }

    askDisplayerToEndSession(sessionId) {
        const { DisplayerService, sessions } = this;

        return new Promise((resolve) => {
            const session = sessions.get(sessionId);

            if (session != null) {
                const { application, displayer, player } = session;

                displayer.send('session-end', {
                    player: player.username,
                    application: application.name,
                })
                    .then(() => DisplayerService.restituteDisplayer(displayer.name))
                    .then(() => this.canStartSession())
                    .catch(() => {});
            }

            resolve();
        });
    }

    askPlayerToEndSession(sessionId) {
        const { PlayerService, sessions } = this;

        return new Promise((resolve) => {
            const session = sessions.get(sessionId);

            if (session != null) {
                const { application, displayer, player } = session;

                player.send('session-end', {
                    displayer: displayer.name,
                    application: application.name,
                })
                    .then(() => PlayerService.kickPlayer(player.username))
                    .then(() => this.canStartSession())
                    .catch(() => {});
            }

            resolve();
        });
    }

    getSession(sessionId) {
        const { sessions } = this;

        return new Promise((resolve, reject) => {
            const session = sessions.get(sessionId);

            if (session != null) {
                resolve(session);
            } else {
                reject();
            }
        });
    }

    getSessions() {
        const { sessions } = this;

        return new Promise((resolve) => {
            resolve([...sessions.values()]);
        });
    }

    /*
    updateSession(sessionId, displayerName) {
        const { sessions, DisplayerService } = this;

        return new Promise((resolve, reject) => {
            const session = sessions.get(sessionId);

            if (session != null) {
                DisplayerService.getDisplayer(displayerName)
                    .then((newDisplayer) => {
                        const { player, application, displayer } = session;

                        Promise.all([
                            player.send('update-displayer', {
                                application: application.name,
                                displayer: displayer.name,
                            }),
                            application.send('update-displayer', {
                                player: player.username,
                                displayer: displayer.name.replace(/[^A-Za-z0-9]/g, ''),
                            }),
                            displayer.send('update-displayer', {
                                player: player.username,
                                application: application.name,
                            }),
                            DisplayerService.restituteDisplayer(displayer),
                        ]).then(() => {

                        });

                        sessions.set(sessionId, displayer);
                    })
                    .catch(() => {
                        reject();
                    });
            } else {
                reject();
            }
        });
    }
    */
};
