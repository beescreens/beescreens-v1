
module.exports = class {
    constructor(debug, ApplicationService, DisplayerService, PlayerService) {
        this.debug = debug('beescreens:message-service');
        this.ApplicationService = ApplicationService;
        this.DisplayerService = DisplayerService;
        this.PlayerService = PlayerService;
    }

    sendMessage(title /* , content, level */) {
        const {
            debug,
            // ApplicationService,
            // DisplayerService,
            // PlayerService
        } = this;

        debug(`Sending message '${title}' to everyone.`);

        /*
        return new Promise((resolve) => {
            Promise.all([
                ApplicationService.getApps(),
                DisplayerService.getDisplayers(),
                PlayerService.getPlayers(),
            ])
                .then((entities) => {
                    entities.forEach(entity => {
                        entity.send();
                    });
                    console.log(values);

                    resolve();
                });
        });
        */
    }

    sendMessageToApp(applicationName, title /* , content, level */) {
        const { debug /* ApplicationService */ } = this;

        debug(`Sending message '${title}' to everyone playing '${applicationName}'.`);
        /*
        return new Promise((resolve, reject) => {
            resolve();
        });
        */
    }

    sendMessageToDisplayer(displayerName, title /* , content, level */) {
        const { debug /* DisplayerService */ } = this;

        debug(`Sending message '${title}' to display '${displayerName}'.`);

        /*
        return new Promise((resolve, reject) => {
            resolve();
        });
        */
    }

    sendMessageToPlayer(username, title, content, level) {
        const { debug, PlayerService } = this;

        return new Promise((resolve, reject) => {
            PlayerService
                .getPlayer(username)
                .then((player) => {
                    player.socket.emit('message', {
                        title,
                        content,
                        level,
                    });

                    debug(`Sending message '${title}' to player '${username}'.`);
                    resolve();
                })
                .catch(() => reject());
        });
    }
};
