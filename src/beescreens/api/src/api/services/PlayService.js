const uniqid = require('uniqid');

module.exports = class {
    constructor(ApplicationService, PlayerService, QueueService, SessionService, Status) {
        this.ApplicationService = ApplicationService;
        this.PlayerService = PlayerService;
        this.QueueService = QueueService;
        this.SessionService = SessionService;
        this.Status = Status;
    }

    play(applicationName) {
        const {
            ApplicationService,
            PlayerService,
            Status,
        } = this;

        return new Promise((resolve, reject) => {
            ApplicationService.getApp(applicationName)
                .then((application) => {
                    if (application.status === Status.ACTIVE.toString()) {
                        let username;

                        do {
                            // TODO - change to friendly usernames.
                            username = uniqid();

                            PlayerService
                                .getPlayer(username)
                                // eslint-disable-next-line no-loop-func
                                .then(() => {
                                    username = null;
                                })
                                // eslint-disable-next-line no-loop-func
                                .catch(() => {});
                        } while (username == null);

                        PlayerService.addPlayer(username);
                        resolve(username);
                    } else {
                        reject();
                    }
                }).catch(() => {
                    reject();
                });
        });
    }

    joinQueue(player, application) {
        const { QueueService } = this;

        return new Promise((resolve) => {
            QueueService
                .addPlayerProcess(player, application)
                .then((position) => {
                    resolve(position);
                });
        });
    }

    displayerDisconnect(displayer) {
        const { DisplayerService, SessionService, Status } = this;

        return new Promise((resolve) => {
            const { sessionId } = displayer;

            if (sessionId != null) {
                Promise.all([
                    SessionService.askPlayerToEndSession(displayer.sessionId),
                    SessionService.askApplicationToEndSession(displayer.sessionId),
                    DisplayerService.updateDisplayer(
                        displayer.name,
                        displayer.width,
                        displayer.height,
                        Status.ACTIVE.toString(),
                    ),
                ])
                    .then(() => SessionService.sessions.delete(sessionId));
            }

            resolve();
        });
    }

    playerDisconnect(player) {
        const { PlayerService, QueueService, SessionService } = this;

        return new Promise((resolve) => {
            const { position, sessionId } = player;

            if (position != null) {
                QueueService
                    .deleteQueueElement(position)
                    .then(() => resolve());
            } else if (sessionId != null) {
                Promise.all([
                    SessionService.askDisplayerToEndSession(player.sessionId),
                    SessionService.askApplicationToEndSession(player.sessionId),
                    PlayerService.kickPlayer(player.username),
                ])
                    .then(() => {
                        SessionService.sessions.delete(sessionId);
                        resolve();
                    });
            }
        });
    }
};
