module.exports = class {
    constructor(mongoose, UserModel, Status) {
        const { Schema } = mongoose;

        const UserSchema = new Schema({
            username: {
                type: String,
                unique: true,
                required: true,
                dropDups: true,
            },
            password: {
                type: String,
                required: true,
            },
            role: {
                type: String,
                required: true,
            },
        });

        this.Record = mongoose.model('User', UserSchema);
        this.UserModel = UserModel;
        this.Status = Status;
    }

    createUser(username, password, role) {
        const { Record, UserModel, Status } = this;

        return new Promise((resolve, reject) => {
            const newUser = new UserModel(
                username,
                password,
                role,
                Status.ACTIVE.toString(),
            );

            const record = new Record(newUser);

            record.save()
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    reject();
                });
        });
    }

    deleteUser(username) {
        return new Promise((resolve, reject) => {
            const { Record } = this;

            Record.findOneAndDelete({ username }, (err) => {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            });
        });
    }

    getUser(username) {
        return new Promise((resolve, reject) => {
            const { Record } = this;

            Record.findOne({ username }, (err, user) => {
                if (user != null) {
                    resolve(user);
                } else {
                    reject();
                }
            });
        });
    }

    getUsers() {
        return new Promise((resolve) => {
            const { Record } = this;

            Record.find({}, (err, users) => {
                resolve(users);
            });
        });
    }

    updateUser(username, password, role) {
        const { UserModel, Record } = this;

        return new Promise((resolve, reject) => {
            const user = new UserModel(
                username,
                password,
                role,
            );

            Record.findOneAndUpdate({ username }, user, (err) => {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            });
        });
    }
};
