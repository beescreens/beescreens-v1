module.exports = class {
    constructor(debug, JwtService, UserService, UserDTO) {
        this.debug = debug('beescreens:login-logout-service');
        this.JwtService = JwtService;
        this.UserService = UserService;
        this.UserDTO = UserDTO;
    }

    login(username, password) {
        const { debug, UserService, UserDTO } = this;

        return new Promise((resolve, reject) => {
            UserService.getUser(username)
                .then((user) => {
                    if (user.password !== password) {
                        debug(`User ${username} has not successfully logged in.`);
                        reject();
                    }

                    debug(`User ${username} has successfully logged in.`);
                    return user;
                })
                .then((user) => {
                    const dto = new UserDTO(user);

                    resolve(dto);
                })
                .catch(() => reject());
        });
    }

    logout(token) {
        return new Promise((resolve) => {
            const { debug, JwtService } = this;

            JwtService
                .addBlacklistedJwt(token)
                .then(() => {
                    debug('A user has successfully logged out.');
                    resolve();
                });
        });
    }
};
