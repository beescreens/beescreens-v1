const Compression = require('compression');
const Cors = require('cors');
const Express = require('express');
const EventEmitter = require('events');
const Helmet = require('helmet');
const History = require('connect-history-api-fallback');
const Http = require('http');
const Path = require('path');
const Swagger = require('swagger-tools');

module.exports = class extends EventEmitter {
    /**
     * The constructor.
     */
    constructor(
        debug,
        swaggerSpec,
        IpHelper,
        JwtHelper,
        publicFolder,
    ) {
        super();

        // Express
        const express = Express();

        // Swagger
        Swagger.initializeMiddleware(swaggerSpec, (middleware) => {
            express.use(
                // Check if IP is not blacklisted
                IpHelper.verify,

                // Compress requests
                Compression(),

                // Add CORS headers
                Cors(),

                // Remove unsecure headers
                Helmet(),

                // Interpret Swagger resources and attach metadata to request
                middleware.swaggerMetadata(),

                // Verify the token
                middleware.swaggerSecurity({
                    Bearer: JwtHelper.verify,
                }),

                // Validate Swagger requests
                middleware.swaggerValidator(),

                // Route validated requests to appropriate controller
                middleware.swaggerRouter({
                    controllers: Path.resolve(__dirname, './controllers'),
                }),

                // Serve the Swagger documents and Swagger UI
                middleware.swaggerUi({
                    swaggerUi: '/api',
                }),

                // Serve static files
                Express.static(
                    Path.resolve(__dirname, publicFolder),
                ),

                // Enable single-page applications to work
                History(),
            );
        });

        express.get('/', (req, res) => {
            res.redirect('/play');
        });

        const instance = Http.createServer(express);

        this.debug = debug('beescreens:rest-server');
        this.express = express;
        this.instance = instance;
    }

    /**
     * Start the server.
     */
    start() {
        const { debug, instance } = this;

        instance.listen(8080, () => {
            debug('Listening on *:8080');
            this.emit('started');
        });
    }

    /**
     * Stop the server.
     */
    stop() {
        const { debug, instance } = this;

        instance.close(() => {
            debug('Server gracefully stopped.');
            this.emit('stopped');
        });
    }
};
