const container = require('../../container');

const {
    MessageService,
    Response204,
    Response404,
} = container.cradle;

module.exports.sendMessage = function sendMessage(req, res) {
    let { message } = req.swagger.params;

    message = message.value;

    MessageService.sendMessage(message)
        .then(() => {
            Response204.send(res);
        });
};

module.exports.sendMessageToApp = function sendMessageToApp(req, res) {
    let { applicationName, message } = req.swagger.params;

    applicationName = applicationName.value;
    message = message.value;

    MessageService.sendMessageToApp(applicationName, message)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.sendMessageToDisplayer = function sendMessageToDisplayer(req, res) {
    let { displayerName, message } = req.swagger.params;

    displayerName = displayerName.value;
    message = message.value;

    MessageService.sendMessageToDisplayer(displayerName, message)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.sendMessageToPlayer = function sendMessageToPlayer(req, res) {
    let { username, message } = req.swagger.params;

    username = username.value;
    message = message.value;

    MessageService.sendMessageToPlayer(username, message)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};
