const container = require('../../container');

const {
    OptionService,
    OptionDTO,
    Response200,
    Response204,
    Response404,
} = container.cradle;

module.exports.changeOptionValue = function changeDisplayersPassword(req, res) {
    let { optionId, option } = req.swagger.params;

    optionId = optionId.value;
    option = option.value;

    OptionService.changeOptionValue(optionId, option.value)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getOptions = function getOptions(req, res) {
    OptionService.getOptions()
        .then((options) => {
            const optionsDTO = [];

            options.forEach((option) => {
                optionsDTO.push(new OptionDTO(option));
            });

            Response200.send(optionsDTO, res);
        });
};
