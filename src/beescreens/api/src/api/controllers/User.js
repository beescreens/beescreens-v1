const container = require('../../container');

const {
    UserDTO,
    UserService,
    Response200,
    Response204,
    Response404,
    Response409,
} = container.cradle;

module.exports.createUser = function createUser(req, res) {
    let { user } = req.swagger.params;

    user = user.value;

    UserService.createUser(user.username, user.password, user.role)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response409.send(res);
        });
};

module.exports.deleteUser = function deleteUser(req, res) {
    let { username } = req.swagger.params;

    username = username.value;

    UserService.deleteUser(username)
        .then(() => {
            Response204.send(res);
        });
};

module.exports.getUser = function getUser(req, res) {
    let { username } = req.swagger.params;

    username = username.value;

    UserService.getUser(username)
        .then((user) => {
            const dto = new UserDTO(user);

            Response200.send(dto, res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getUsers = function getUsers(req, res) {
    UserService.getUsers()
        .then((users) => {
            const usersDTO = [];

            users.forEach((user) => {
                usersDTO.push(new UserDTO(user));
            });

            Response200.send(usersDTO, res);
        });
};

module.exports.updateUser = function updateUser(req, res) {
    let { username, user } = req.swagger.params;

    username = username.value;
    user = user.value;

    UserService.updateUser(username, user.password, user.role)
        .then(() => {
            Response204.send(res);
        });
};
