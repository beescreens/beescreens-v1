const container = require('../../container');

const {
    JwtService,
    LoginLogoutService,
    Response200,
    Response204,
    Response401,
    WrongUsernamePasswordError,
} = container.cradle;

module.exports.login = function login(req, res) {
    let { credentials } = req.swagger.params;

    credentials = credentials.value;

    const { username, password } = credentials;

    LoginLogoutService.login(username, password)
        .then(dto => JwtService.sign({
            id: username,
            role: dto.role,
        }))
        .then((token) => {
            Response200.send({
                id: username.replace(/[^A-Za-z0-9]/g, ''),
                jwt: token,
            }, res);
        })
        .catch(() => {
            Response401.send(WrongUsernamePasswordError.message, res);
        });
};

module.exports.logout = function logout(req, res) {
    const authorization = req.auth;

    LoginLogoutService.logout(authorization)
        .then(() => {
            Response204.send(res);
        });
};
