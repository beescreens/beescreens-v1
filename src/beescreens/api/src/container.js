const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

const {
    asClass, asFunction, asValue, createContainer, InjectionMode, Lifetime,
} = require('awilix');
const debug = require('debug');
const fs = require('fs');
const jsyaml = require('js-yaml');
const path = require('path');

const { Database, Mongoose, MongooseConnection } = require('./api/Database');
const IoServer = require('./api/IoServer');
const PeerServer = require('./api/PeerServer');
const RestServer = require('./api/RestServer');

const container = createContainer({
    injectionMode: InjectionMode.PROXY,
});

const EnumValue = name => Object.freeze({ toString: () => name });

// Register parameters
container.register({
    adminUsername: asValue(process.env.ADMIN_USERNAME),
    adminPassword: asValue(process.env.ADMIN_PASSWORD),
    applicationsPath: asValue(path.resolve(__dirname, './../../apps')),
    publicFolder: asValue(path.resolve(__dirname, './../../public')),
    displayersPassword: asValue(process.env.DISPLAYERS_PASSWORD),
    dbUsername: asValue(process.env.DB_USERNAME),
    dbPassword: asValue(process.env.DB_PASSWORD),
    dbHostname: asValue(process.env.DB_HOSTNAME),
    dbName: asValue(process.env.DB_NAME),
    jwtSecret: asValue(process.env.JWT_SECRET),
    jwtExpiration: asValue(process.env.JWT_EXPIRATION),
    swaggerSpec: asValue(
        jsyaml.safeLoad(
            fs.readFileSync(
                path.resolve(__dirname, './api/swagger/api.yml'), 'utf8',
            ),
        ),
    ),
    Status: asValue(
        Object.freeze({
            ACTIVE: EnumValue('active'),
            INACTIVE: EnumValue('inactive'),
            BANNED: EnumValue('banned'),
        }),
    ),
    EntityTypes: asValue(
        Object.freeze({
            PLAYER: EnumValue('player'),
            DISPLAYER: EnumValue('displayer'),
            APPLICATION: EnumValue('application'),
        }),
    ),
});

// Register responses
container.loadModules([path.resolve(__dirname, './api/utils/responses/**/Response*.js')], {
    resolverOptions: {
        register: asClass,
        injectionMode: InjectionMode.CLASSIC,
        lifetime: Lifetime.SINGLETON,
    },
});

// Register errors
container.loadModules([path.resolve(__dirname, './api/utils/errors/**/*Error.js')], {
    resolverOptions: {
        register: asClass,
        injectionMode: InjectionMode.CLASSIC,
        lifetime: Lifetime.SINGLETON,
    },
});

// Register utils
container.loadModules([path.resolve(__dirname, './api/utils/*.js')], {
    resolverOptions: {
        register: asFunction,
    },
});

// Register helpers
container.loadModules([path.resolve(__dirname, './api/helpers/**/*.js')], {
    resolverOptions: {
        register: asFunction,
    },
});

// Register main classes
container.register({
    database: asClass(Database).classic().singleton(),
    ioServer: asClass(IoServer).classic().singleton(),
    peerServer: asClass(PeerServer).classic().singleton(),
    restServer: asClass(RestServer).classic().singleton(),
    mongooseConnection: asValue(MongooseConnection),
    mongoose: asValue(Mongoose),
    debug: asValue(debug),
});

// Register DTO
container.loadModules([path.resolve(__dirname, './api/dto/**/*DTO.js')], {
    resolverOptions: {
        register: asFunction,
    },
});

// Register models
container.loadModules([path.resolve(__dirname, './api/models/**/*Model.js')], {
    resolverOptions: {
        register: asFunction,
    },
});

// Register services
container.loadModules([path.resolve(__dirname, './api/services/**/*Service.js')], {
    resolverOptions: {
        register: asClass,
        injectionMode: InjectionMode.CLASSIC,
        lifetime: Lifetime.SINGLETON,
    },
});

// Export container
module.exports = container;
