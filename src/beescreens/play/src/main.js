import Vue from 'vue';
import Toasted from 'vue-toasted';
import App from './App.vue';
import router from './router';
import store from './store';

import Comm from './Comm';
import './assets/scss/main.scss';

Vue.config.productionTip = false;

Vue.prototype.appComm = Comm;

Vue.use(Toasted, {
    position: 'top-center',
    duration: 4000,
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
