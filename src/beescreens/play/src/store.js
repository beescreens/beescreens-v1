/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import apps from './AppLoader';

Vue.use(Vuex);

const wrapper = {
    namespaced: true,
    state: {
        apps: [],
        selectedApp: null,
        displayer: null,
    },
    getters: {
        displayer(state) {
            return state.displayer;
        },
        serverUrl() {
            return window.location.origin;
            // return 'http://localhost:8080';
        },
        appManifest: state => appName => state.apps
            .filter(app => app.manifest.name === appName)[0].manifest,
        appConfig: (state, getters, rootState, rootGetters) => (appName) => {
            const manifest = getters.appManifest(appName);
            return manifest.client && manifest.client.store && manifest.client.store.sendConfig
                ? rootGetters[`${manifest.client.store.namespace}/${manifest.client.store.sendConfig}`] : {};
        },
    },
    mutations: {
        setDisplayer(state, displayer) {
            state.displayer = displayer;
        },
        pushApp(state, app) {
            state.apps.push(app);
        },
        selectApp(state, app) {
            state.selectedApp = app;
        },
    },
    actions: {
        configureApp({ getters, dispatch }, appName) {
            return new Promise((resolve) => {
                const manifest = getters.appManifest(appName);
                console.log(`${manifest.client.store.namespace}/${manifest.client.store.configure}`);
                if (manifest.client && manifest.client.store && manifest.client.store.configure) {
                    dispatch(`${manifest.client.store.namespace}/${manifest.client.store.configure}`, null, { root: true }).then(() => {
                        resolve();
                    });
                } else resolve();
            });
        },
    },
};

const modules = {
    wrapper,
};
apps.forEach((app) => {
    if (app.store !== null) {
        modules[app.manifest.client.store.namespace] = {
            namespaced: true,
            ...app.store,
        };
    }
});

const store = new Vuex.Store({
    modules,
});

apps.forEach((app) => {
    store.commit('wrapper/pushApp', {
        key: app.key,
        manifest: app.manifest,
        logo: app.logo,
    });
});

export default store;
