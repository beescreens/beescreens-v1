// Load list of apps
const apps = new Map();

// Load all applications manifests
const manifestContext = require.context('../../apps/', true, /Manifest\.json/);
// Declare a global requiring context which allows to load any kind of file
const loadContext = require.context('../../apps/', true, /^(?!.*(?:node_modules|server)).*$/);

console.log(manifestContext);
// iterate over each manifest
manifestContext.keys().forEach((filename) => {
    console.log(filename);
    const appKey = filename.split('/')[1];
    const manifest = manifestContext(filename);
    const app = {
        key: appKey,
        manifest,
        root: loadContext(`./${appKey}/${manifest.client.root}`).default,
        logo: loadContext(`./${appKey}/${manifest.logo}`),
        store: null,
    };
    // Load required component
    if (manifest.client.store !== undefined) {
        app.store = loadContext(`./${appKey}/${manifest.client.store.module}`).default;
    }
    apps.set(appKey, app);
});

export default apps;
