import io from 'socket.io-client';
import bus from './event';
import store from './store';

/**
 Object permettant aux applications de communiquer avec le serveur
 */
class Comm {
    constructor() {
        this.socket = null;
    }

    async connect(url, jwt, params) {
        return new Promise((resolve, reject) => {
            this.socket = io.connect(`${store.getters['wrapper/serverUrl']}/api/players`);
            this.socket.on('connect', () => {
                console.log('connected from comm');
                this.socket.emit('authentication', { jwt, params });
                this.socket.on('queue', (data) => {
                    console.log('got queue', data);
                    bus.$emit('queue', data);
                });
                this.socket.on('receive', (data) => {
                    console.log('got receive');
                    console.log(data);
                });
                this.socket.on('message', (data) => {
                    console.log('got message');
                    console.log(data);
                    bus.$emit('message', data);
                });
                this.socket.on('session-ready', () => {
                    bus.$emit('session-ready');
                });
                this.socket.on('session-go', () => {
                    console.log('got session go');
                    bus.$emit('session-go');
                });
                this.socket.on('done', (data) => {
                    console.log('got done');
                    console.log(data);
                });
                this.socket.on('session-end', () => {
                    bus.$emit('session-end');
                    this.socket.close();
                    this.socket = null;
                });
                this.socket.on('disconnect', () => {
                    bus.$emit('disconnected');
                    this.socket.close();
                    this.socket = null;
                });
                this.socket.on('unauthorized', () => {
                    console.log('got unauthorized');
                    this.socket.close();
                    this.socket = null;
                    reject();
                });
                this.socket.on('authenticated', () => {
                    console.log('got authenticated');
                    // this.socket.emit('params', { params });
                    resolve(this.socket);
                });
            });
        });
    }

    disconnect() {
        this.socket.close();
    }

    send(data) {
        console.log(data);
        if (this.socket !== null && this.socket.connected) {
            this.socket.emit('player-data', data);
        }
    }
}

export default new Comm();
