exports.config = {
  tests: './test/*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
      url: 'localhost:8081',
      browser: 'firefox'
    }
  },
  include: {},
  bootstrap: null,
  mocha: {},
  name: 'codeceptJS'
}