const fetch = require('node-fetch');

const container = require('../container');

describe('Option', () => {
    const {
        beescreensHostname,
        adminUsername,
        adminPassword,
        expect,
    } = container.cradle;

    const url = `http://${beescreensHostname}/api`;

    const admin = {
        username: adminUsername,
        password: adminPassword,
    };

    let token = null;

    it('Can login', async () => {
        await fetch(`${url}/login`, {
            method: 'post',
            body: JSON.stringify(admin),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                res.status.should.equal(200);

                return res.json();
            })
            .then((body) => {
                body.jwt.should.exist();
                token = body.jwt;
            });
    });

    it('Can get all options', async () => {
        await fetch(`${url}/options`, {
            method: 'get',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then(res => res.json())
            .then((options) => {
                options.forEach((option) => {
                    expect(option).to.have.property('id');
                    expect(option).to.have.property('name');
                    expect(option).to.have.property('description');
                    expect(option).to.have.property('value');
                });
            });
    });

    it('Can change the value of an option ("accept_new_displayers")', async () => {
        await fetch(`${url}/options/accept_new_displayers`, {
            method: 'patch',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                value: 'false',
            }),
        })
            .then((res) => {
                res.status.should.equal(204);

                return fetch(`${url}/options`, {
                    method: 'get',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
            })
            .then(res => res.json())
            .then((options) => {
                const option = options.find(o => o.id === 'accept_new_displayers');

                option.value.should.equal('false');

                return fetch(`${url}/options/accept_new_displayers`, {
                    method: 'patch',
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        value: 'true',
                    }),
                });
            })
            .then((res) => {
                res.status.should.equal(204);
            });
    });

    it('An unknown option shoud return 404 ("unknown_option")', async () => {
        await fetch(`${url}/options/unknown_option`, {
            method: 'patch',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                value: 'false',
            }),
        })
            .then((res) => {
                res.status.should.equal(404);
            });
    });
});
