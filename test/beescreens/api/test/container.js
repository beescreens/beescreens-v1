const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

const {
    asValue, createContainer, InjectionMode,
} = require('awilix');

const chaiConfig = require('../config/chai-config');

const container = createContainer({
    injectionMode: InjectionMode.PROXY,
});

// Register parameters
container.register({
    expect: asValue(chaiConfig.expect),
    should: asValue(chaiConfig.should),
    beescreensHostname: asValue(process.env.BEESCREENS_HOSTNAME),
    adminUsername: asValue(process.env.ADMIN_USERNAME),
    adminPassword: asValue(process.env.ADMIN_PASSWORD),
    displayersPassword: asValue(process.env.DISPLAYERS_PASSWORD),
});

// Export container
module.exports = container;
