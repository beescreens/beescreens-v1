# Minutes

Here they are, all the minutes of **BeeScreens**

## Thursday - 11 October 2018

### Introduction

We talked about our works, endpoints and we clarify a misunderstanding related to the project architecture.

Vincent was absent.

### Presentation

Guillaume showed us his work about the drawing application. (locally for the moment) The application allow to draw lines in differents sizes and colors.

I (Xavier) showed  my work about routing streams, we fixed the css to resolve the margins problems. The video is now on fullscreen in kiosk mode.

### Endpoints

We talked about new and old endpoints, Ludovic presented us some solutions.

### Architecture

There was a misunderstanding with the architecture. The final choice is that each application provide a video stream in any way they want. They can use canvas with headless browser, ffmpeg etc..

### TODO

- Continue to define the server API (**Ludovic & Vincent**)
- Continue the drawing application on the server (**Guillaume**)
- Create the page that will allow the player to chose an application in a list of applications. (**Xavier**)

## Thursday - 4 October 2018

### Introduction

Today, we had to end some issues from the "Preparation" milestone and define which technology we're going to use for the streaming.

### Workflow

We're going tu use a MongoDB Express Vue.js et NodeJS (MEVN) technology stack not dockerized. We rely on the GitLab pipeline which is supposed to build docker image at the end of the project.

### Streaming

[`HTMLCanvasElement.captureStream()`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/captureStream) will be the center of our streaming feature. It returns a [`MediaStream`](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream) which includes a [`CanvasCaptureMediaStreamTrack`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasCaptureMediaStreamTrack) containing a real-time video capture of the canvas's contents.

We change the architecture of the server a little, like the `pages/architecture.md` will show it. But the main idea is always that the displayer will have to only display the result and not calculate it.

### A story

So basically, the player will send JSON of his moves and options to the server, the server will simulate the application in a headless browser in a canvas, generate a stream, and send it to the displayer.

### Issues cleaning

We reviewed all the issues to clean the project, see the priority, close some issues,.. etc

### TODO

- Vincent & Ludovic
  - Node server
- Xavier
  - Routing streams
- Guillaume
  - VueJS and applications

## Thursday - 4 October 2018

### Introduction

Today, we had to end some issues from the "Preparation" milestone and define which technology we're going to use for the streaming.

### Workflow

We're going tu use a MongoDB Express Vue.js et NodeJS (MEVN) technology stack not dockerized. We rely on the GitLab pipeline which is supposed to build docker image at the end of the project.

### Streaming

[`HTMLCanvasElement.captureStream()`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/captureStream) will be the center of our streaming feature. It returns a [`MediaStream`](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream) which includes a [`CanvasCaptureMediaStreamTrack`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasCaptureMediaStreamTrack) containing a real-time video capture of the canvas's contents.

We change the architecture of the server a little, like the `pages/architecture.md` will show it. But the main idea is always that the displayer will have to only display the result and not calculate it.

### A story

So basically, the player will send JSON of his moves and options to the server, the server will simulate the application in a headless browser in a canvas, generate a stream, and send it to the displayer.

### Issues cleaning

We reviewed all the issues to clean the project, see the priority, close some issues,.. etc

### TODO

- Vincent & Ludovic
  - Node server
- Xavier
  - Routing streams
- Guillaume
  - VueJS and applications

## Thursday - 27 september 2018

### Introduction

We ended up in G04 to talk about the foundations of the project. We put a big emphasis on project management.

Ludovic Delafontaine presented us the different functionalities proposed by Gitlab, and proposed us to quickly define a working method for the project

### GitLab

**ESLint**, will be used as absolute dictator of our way of coding, Guillaume Hochet imposes the use of the **semicolon** in javascript, so we all agree to use **4 spaces** for tabs.

**Issues**, Contrary to what one believes, the Issues do not exist only to indicate the problems, but for all that is related to the project, that is the useful links, the features, etc.

**Only one repository**, we had two for now, but we found it useful to put the project and the docs (site) in the same place.

**Coverage**, We talked about it and everyone is motivated to make a code of good quality and well tested.

### BeeScreens

We redefined once again the features of our application :

- Screen identifying

  Possibility when you can finally draw, to identify which screen is assigned to us. Vincent proposed a frame on the screen the same color as the bottom of our app on the phone, but not user-friendly for color-blind, Guillaume and Ludovic propose a kind of flash, but everyone can use the flash in same time ==> epileptic hazard

- Admin login

  We will have the opportunity to log into admin for reasons of

  - Censorship
  - Parameter changes, such as the number of players, screens, session time

- Session limitation

  Limit the playing time, or make this option flexible, for example, during a music festival, between concerts, a shorter time because of the influx of people, and put a longer playing time during concerts when there are fewer people available to play

- ScreenSaver

  It will happen that screens are not used from time to time, and you have to use these screens for something else, the app must show a constant use, here are the way to fill these screens

  - advertising
  - oldest usage, oldest game play, etc..

- App choices

  The user can choose different games, such as *drawing* or playing *Tic Tac Toe*

- Responsive design

- Moderation

- For drawing, we only have one user per screen

- Splitscreen

  Ability to have a user for multiple screens.

- Define an application template

  Open source requires, anyone should be able to add applications to our server. So we have to define a template of what we receive from responsive app and what we send to the displayer

- Middleware

### TODO

- Server Node **Vincent & Ludo**

  mapping etc..

  - Page User
  - Page Admin
  - Page Displayer

- Streaming **Xavier**

  research and documentation

- Communication Terminal/server **Vincent**

  - Websocket
  - Json

- DB **Xavier**

  not a big priority

  - MangoDB

- Web app **Guillaume**

  - Drawing

### Conclusion

We are all very motivated for this project, and happy to take the time to lay the foundation of the project, so that the development is going at best.
