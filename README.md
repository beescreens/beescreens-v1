# BeeScreens - Be the screens

Interactive screens for events.

## Getting started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to deploy the project on a live system.

### Prerequisites

- [`docker`](https://www.docker.com/) must be installed.
- [`docker-compose`](https://www.docker.com/) must be installed.
- [`git`](https://git-scm.com/) must be installed.

### Install the application


### Prepare the application

```sh
# Clone the repository
git clone git@gitlab.com:beescreens/beescreens.git

# Move to the cloned directory
cd beescreens

# Initialize the submodules (the third-party apps)
git submodule update --init

cd src/beescreens/homepage
git checkout master
git pull

# Do this for each third-party app
cd src/beescreens/apps/<third-party app>
git checkout master
git pull

mv .env.dist .env # Copy and edit the environment variables
```

### Deploy the application

```sh
docker-compose up # Start the infrastructure
```

The application is now deployed:

- [localhost:8080](http://localhost:8080): Access the application.
- [localhost](http://localhost): Access the homepage of the application.

## Running the tests

```sh
cd beescreens # Move to the cloned directory

mv .env.dist .env # Copy and edit the environment variables

docker-compose -f docker-compose-testing.yml up beescreens-test # The infrastructure starts and is tested
```

## Built with :heart: using...

### Server side

- [NodeJS](https://nodejs.org) as the server's core technology.
- [Express](https://expressjs.com/) as the web framework.
- [ESLint](https://github.com/jonathantneal/eslit) as the linter for the project.
- [Mocha](https://mochajs.org/) as the testing framework.
- [Chai](https://www.chaijs.com/) as the assertion library.
- [MongoDB](https://www.mongodb.com/) as the database.
- [Mongoose](https://mongoosejs.com/) as the Object-Document Mapping (ODM).
- Many other Node's libraries...

### Client side
The client side will certainly use the following technologies only to display the available applications:

- [webpack](https://webpack.js.org/) to bundle all the dependencies of a web application.
- [Vue.js](https://vuejs.org/) as the the front-end framework.

#### Third-party applications
As our framework will "only" allow to deploy, view, render and moderate applications, the
developers are more than welcome to develop fun and interactive ways to play with screens.

The client and server's logic must be implemented by the developer so it's up to
her/him which technology she/he wants to use.

##### Drawing application

- [Fabric.js](http://fabricjs.com/) to draw and manipulate images, forms, painting, etc

## How does it work ?

![](docs/ressources/architecture.png)

## Contributing

See issue #13.

## Authors

This project was made at [HEIG-VD](https://heig-vd.ch/), Switzerland and was
brought to you by:

* Ludovic Delafontaine
* Vincent Guidoux
* Guillaume Hochet
* Xavier Vaz Afonso

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md)
file for details.
